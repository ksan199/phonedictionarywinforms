﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class BaseService
    {
        protected T GetService<T>()
            where T : class
        {
            return NinjectProgram.Kernel.Get<T>();
        }
    }
}
