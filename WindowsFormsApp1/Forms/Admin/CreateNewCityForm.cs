﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters;

namespace WindowsFormsApp1.Forms.Admin
{
    public partial class CreateNewCityForm : Form
    {
        public QueriesTableAdapter queriesTableAdapter { get; set; } = new QueriesTableAdapter();
        public CreateNewCityForm()
        {
            InitializeComponent();
        }

        private void CreateNewCityForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "phoneDictionaryDataSet.Regions". При необходимости она может быть перемещена или удалена.
            this.regionsTableAdapter.Fill(this.phoneDictionaryDataSet.Regions);

        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                queriesTableAdapter.AddCity(cityNameTextBox.Text);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch
            {
                MessageBox.Show("Ошибка при добавлении города. Возможно, указанный город уже существует, или регион указан неверно");
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
