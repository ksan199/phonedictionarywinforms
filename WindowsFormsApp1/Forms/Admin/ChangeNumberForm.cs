﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Forms
{
    public partial class ChangeNumberForm : Form
    {
        public ChangeNumberForm(string numberPlaceholder = "")
        {
            InitializeComponent();
            maskedTextBox.Text = numberPlaceholder;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (maskedTextBox.MaskCompleted)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                errorLabel.Visible = true;
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
