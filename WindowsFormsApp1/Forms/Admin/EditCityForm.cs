﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters;

namespace WindowsFormsApp1.Forms.Admin
{
    public partial class EditCityForm : Form
    {
        private string _oldCityName;

        public QueriesTableAdapter queriesTableAdapter { get; set; } = new QueriesTableAdapter();
        public EditCityForm(string oldCityName)
        {
            InitializeComponent();
            _oldCityName = oldCityName;
        }

        private void EditCityForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "phoneDictionaryDataSet.Regions". При необходимости она может быть перемещена или удалена.
            this.regionsTableAdapter.Fill(this.phoneDictionaryDataSet.Regions);
            cityNameTextBox.Text = _oldCityName;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                queriesTableAdapter.UpdateCity(
                        _oldCityName,
                        cityNameTextBox.Text
                    );
                DialogResult = DialogResult.OK;
                Close();
            }
            catch
            {
                MessageBox.Show("Ошибка при редактировании региона. Убедитесь, что указанный регион существует в базе.");
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
