﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters;

namespace WindowsFormsApp1.Forms.Admin
{
    public partial class CreateNewStreetForm : Form
    {
        public QueriesTableAdapter queriesTableAdapter { get; set; } = new QueriesTableAdapter();
        public CreateNewStreetForm()
        {
            InitializeComponent();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                queriesTableAdapter.AddStreet(streetNameTextBox.Text);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch
            {
                MessageBox.Show("Ошибка при добавлении улицы");
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
