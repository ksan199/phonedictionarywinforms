﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Services;

namespace WindowsFormsApp1.Forms.Admin
{
    public partial class CreateNewAccountForm : BaseForm
    {
        private AccountManager _accountManager;
        public Dictionary<string, string> RoleNameToDBRoleValue { get; set; } = new Dictionary<string, string>();
        public string ADMIN_ROLE_NAME { get; set; } = "Админ";
        public string REDACTOR_ROLE_NAME { get; set; } = "Редактор";

        public CreateNewAccountForm()
        {
            InitializeComponent();
            Init();
            RoleNameToDBRoleValue.Add(ADMIN_ROLE_NAME, AuthService.ADMIN_ROLE);
            RoleNameToDBRoleValue.Add(REDACTOR_ROLE_NAME, AuthService.REDACTOR_ROLE);
            rolesCheckedListBox.Items.AddRange(new string[]{ ADMIN_ROLE_NAME, REDACTOR_ROLE_NAME });
        }

        private void Init()
        {
            _accountManager = GetService<AccountManager>();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (loginTextBox.Text.Length >= 2 && passwordTextBox.Text.Length >= 6)
            {
                try
                {
                    List<string> selectedRoles = new List<string>();
                    foreach (string role in rolesCheckedListBox.CheckedItems)
                    {
                        selectedRoles.Add(RoleNameToDBRoleValue[role]);
                    }

                    _accountManager.CreateUser(
                        loginTextBox.Text,
                        passwordTextBox.Text,
                        selectedRoles
                        );

                    DialogResult = DialogResult.OK;
                    Close();

                }
                catch
                {
                    MessageBox.Show("Ошибка при создании пользователя");
                }
                
            }
                
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
