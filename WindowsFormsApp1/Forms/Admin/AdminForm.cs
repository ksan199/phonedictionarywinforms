﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Forms;
using WindowsFormsApp1.Forms.Admin;
using WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters;
using WindowsFormsApp1.Services;

namespace WindowsFormsApp1
{
    public partial class AdminForm : BaseForm
    {
        private AccountManager _accountManager;

        /// <summary>
        /// Сигнализирует о необходимости выхода в меню при нажатии кнопки exit на форме
        /// </summary>
        public bool isRelaunchRequierd { get; set; }
        public QueriesTableAdapter queriesTableAdapter { get; set; } = new QueriesTableAdapter();
        public AuthService _authService { get; set; }
        /// <summary>
        /// Номера из этого поля удаляются из базы при сохранении изменений
        /// </summary>
        public List<string> NumbersToDelete { get; set; } = new List<string>();
        public AdminForm()
        {
            InitializeComponent();

            _authService = GetService<AuthService>();
            _accountManager = GetService<AccountManager>();

            birthdayDateTimePicker.Checked = false;

            if (!_authService.Roles.Contains(AuthService.REDACTOR_ROLE))
            {
                personsPage.Enabled = false;
                regionsTabPage.Enabled = false;
                citiesPage.Enabled = false;
                streetsPage.Enabled = false;
            }
            if (!_authService.Roles.Contains(AuthService.ADMIN_ROLE))
            {
                accessPage.Enabled = false;
            }
        }

        private void AdminForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!isRelaunchRequierd)
            {
                Application.Exit();
            }
        }

        private void AdminForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "phoneDictionaryDataSet.Streets". При необходимости она может быть перемещена или удалена.
            FetchStreets();
            streetsComboBox.Text = "";
            // TODO: данная строка кода позволяет загрузить данные в таблицу "phoneDictionaryDataSet.Cities". При необходимости она может быть перемещена или удалена.
            FetchCities();
            citiesComboBox.Text = "";
            // TODO: данная строка кода позволяет загрузить данные в таблицу "phoneDictionaryDataSet.Regions". При необходимости она может быть перемещена или удалена.
            FetchRegionsAndSetToEmpty();
            // TODO: данная строка кода позволяет загрузить данные в таблицу "phoneDictionaryDataSet.GetAllUsersWithRoles". При необходимости она может быть перемещена или удалена.
            this.getAllUsersWithRolesTableAdapter.Fill(this.phoneDictionaryDataSet.GetAllUsersWithRoles);
            FetchPersonsByTextBoxValues();
        } 

        /// <summary>
        /// Загружает список регионов и обнуляет выделенный регион во вкладке менеджмента
        /// </summary>
        private void FetchRegionsAndSetToEmpty()
        {
            FetchRegions();
            regionsComboBox.Text = "";           
        }

        /// <summary>
        /// Загружает список регионов 
        /// </summary>
        private void FetchRegions()
        {
            this.regionsTableAdapter.FillExceptEmpty(this.phoneDictionaryDataSet.Regions);
        }

        private void FetchCities()
        {
            this.citiesTableAdapter.Fill(this.phoneDictionaryDataSet.Cities);
        }

        private void FetchStreets()
        {
            this.streetsTableAdapter.Fill(this.phoneDictionaryDataSet.Streets);
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _authService.Logout();
            isRelaunchRequierd = true;
            if (Owner != null)
            {
                ((LaunchForm)Owner).onChildFormClose();
            }
            Close();
        }

        private void backToMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isRelaunchRequierd = true;
            if (Owner != null)
            {
                ((LaunchForm)Owner).onChildFormClose();
            }
            Close();
        }

        #region personsTab
        private void addNumberButton_Click(object sender, EventArgs e)
        {
            if (phoneMaskedTextBox.MaskCompleted)
                if (!phoneNumbersListBox.Items.Contains(phoneMaskedTextBox.Text))
                {
                    phoneNumbersListBox.Items.Add(phoneMaskedTextBox.Text);
                    phoneMaskedTextBox.Text = "";
                }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            FetchPersonsByTextBoxValues();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            DateTime? birthday = birthdayDateTimePicker.Checked ?
                birthdayDateTimePicker.Value.Date :
                birthday = null;
            int PersonId;

            if (phoneNumbersListBox.Items.Count > 0)
            {
                if (
                    !String.IsNullOrEmpty(firstNameTextBox.Text.Trim()) && 
                    !String.IsNullOrEmpty(lastNameTextBox.Text.Trim()) &&
                    !String.IsNullOrEmpty(patronymicTextBox.Text.Trim())
                    )
                {
                    PersonId = (int)queriesTableAdapter.InsertPersonByParams(
                        firstNameTextBox.Text,
                        lastNameTextBox.Text,
                        patronymicTextBox.Text,
                        birthday,
                        regionsComboBox.Text,
                        citiesComboBox.Text,
                        streetsComboBox.Text,
                        houseNumberTextBox.Text,
                        apartmentNumberTextBox.Text
                    );

                    foreach (string number in phoneNumbersListBox.Items)
                    {
                        queriesTableAdapter.AddNumber(PersonId, number);
                    }

                    FetchPersonsByTextBoxValues();
                }
            }
        }

        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            deletePersonButton.Enabled = true;
            saveChangesButton.Enabled = true;
            addButton.Enabled = false;

            // Очистка кэша номеров, которые следовало удалить при сохранении изменений
            NumbersToDelete.Clear();

            var row = (DataRowView)getPersonsByParamsBindingSource.Current;

            firstNameTextBox.Text = row[dataGridViewFirstNameColumn.DataPropertyName].ToString();
            lastNameTextBox.Text = row[dataGridViewLastNameColumn.DataPropertyName].ToString();
            patronymicTextBox.Text = row[dataGridViewPatronymicColumn.DataPropertyName].ToString();
            birthdayDateTimePicker.Text = row[dataGridViewBirthdayColumn.DataPropertyName].ToString();
            regionsComboBox.Text = row[dataGridViewRegionColumn.DataPropertyName].ToString();
            citiesComboBox.Text = row[dataGridViewCityColumn.DataPropertyName].ToString();
            streetsComboBox.Text = row[dataGridStreetColumn.DataPropertyName].ToString();
            houseNumberTextBox.Text = row[dataGridViewHouseNumberColumn.DataPropertyName].ToString();
            apartmentNumberTextBox.Text = row[dataGridViewApartmentNumberColumn.DataPropertyName].ToString();

            phoneNumbersListBox.Items.Clear();
            changePhoneNumberButton.Enabled = false;
            try
            {
                var selectedNumbers = row[dataGridViewNumbersColumn.DataPropertyName]
                    .ToString()
                    .Split(',');
                
                foreach (var number in selectedNumbers)
                {
                    if (!String.IsNullOrEmpty(number))
                        phoneNumbersListBox.Items.Add(number.Trim());
                }
            }
            catch
            {

            }
        }

        private void FetchPersonsByTextBoxValues()
        {
            DateTime? birthday;
            if (birthdayDateTimePicker.Checked)
            {
                birthday = birthdayDateTimePicker.Value.Date;
            }
            else
            {
                birthday = null;
            }
            getPersonsByParamsTableAdapter.Fill(
                        this.phoneDictionaryDataSet.GetPersonsByParams,
                        firstNameTextBox.Text,
                        lastNameTextBox.Text,
                        patronymicTextBox.Text,
                        birthday,
                        regionsComboBox.Text,
                        citiesComboBox.Text,
                        streetsComboBox.Text,
                        houseNumberTextBox.Text,
                        apartmentNumberTextBox.Text
                    );
        }

        private void deletePhoneNumberButton_Click(object sender, EventArgs e)
        {
            var numberToRemove = phoneNumbersListBox.SelectedItem.ToString();

            // Удаляю номер из списка и сохраняю в кэш для последующего удаления из базы
            phoneNumbersListBox.Items.Remove(numberToRemove);
            NumbersToDelete.Add(numberToRemove);
        }

        private void phoneNumbersListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (phoneNumbersListBox.SelectedIndex >= 0)
            {
                changePhoneNumberButton.Enabled = true;
                deletePhoneNumberButton.Enabled = true;
            }
            else
            {
                changePhoneNumberButton.Enabled = false;
                deletePhoneNumberButton.Enabled = false;
            }
        }

        private void changePhoneNumberButton_Click(object sender, EventArgs e)
        {
            var changeNumberDialog = new ChangeNumberForm(phoneNumbersListBox.SelectedItem.ToString());

            var dialogResult = changeNumberDialog.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                var numberToRemove = phoneNumbersListBox.SelectedItem.ToString();
                phoneNumbersListBox.Items.Remove(numberToRemove);
                NumbersToDelete.Add(numberToRemove);

                phoneNumbersListBox.Items.Add(changeNumberDialog.maskedTextBox.Text);
            }
        }

        private void saveChangesButton_Click(object sender, EventArgs e)
        {
            var personId = (int)((DataRowView)getPersonsByParamsBindingSource.Current)[idDataGridViewTextBoxColumn.DataPropertyName];

            if (
                    phoneNumbersListBox.Items.Count > 0 &&
                    !String.IsNullOrEmpty(firstNameTextBox.Text.Trim()) &&
                    !String.IsNullOrEmpty(lastNameTextBox.Text.Trim()) &&
                    !String.IsNullOrEmpty(patronymicTextBox.Text.Trim())
                    )
            {
                queriesTableAdapter.UpdatePersonByParams(
                personId,
                firstNameTextBox.Text,
                lastNameTextBox.Text,
                patronymicTextBox.Text,
                birthdayDateTimePicker.Value.Date,
                regionsComboBox.Text,
                citiesComboBox.Text,
                streetsComboBox.Text,
                houseNumberTextBox.Text,
                apartmentNumberTextBox.Text
            );

                foreach (var number in NumbersToDelete)
                {
                    queriesTableAdapter.DeleteNumber(personId, number);
                }

                foreach (string number in phoneNumbersListBox.Items)
                {
                    queriesTableAdapter.AddNumber(personId, number);
                }

                // Переход в режим добавления записи
                saveChangesButton.Enabled = false;
                addButton.Enabled = true;
                deletePersonButton.Enabled = false;

                FetchPersonsByTextBoxValues();
            }
        }

        private void deletePersonButton_Click(object sender, EventArgs e)
        {
            queriesTableAdapter.DeletePersonById((int)((DataRowView)getPersonsByParamsBindingSource.Current)[idDataGridViewTextBoxColumn.DataPropertyName]);

            // Переход в режим добавления записи
            saveChangesButton.Enabled = false;
            addButton.Enabled = true;
            deletePersonButton.Enabled = false;

            FetchPersonsByTextBoxValues();
        }

        private void clearFieldsButton_Click(object sender, EventArgs e)
        {
            firstNameTextBox.Text = "";
            lastNameTextBox.Text = "";
            patronymicTextBox.Text = "";
            birthdayDateTimePicker.Checked = false;
            regionsComboBox.Text = "";
            citiesComboBox.Text = "";
            streetsComboBox.Text = "";
            phoneNumbersListBox.Items.Clear();
            phoneMaskedTextBox.Text = "";
            apartmentNumberTextBox.Text = "";
            houseNumberTextBox.Text = "";
            NumbersToDelete.Clear();

            saveChangesButton.Enabled = false;
            deletePersonButton.Enabled = false;
            changePhoneNumberButton.Enabled = false;
            deletePhoneNumberButton.Enabled = false;
            addButton.Enabled = true;
        }

        private void streetsComboBox_TextChanged(object sender, EventArgs e)
        {
            if (streetsComboBox.Text == "")
            {
                houseNumberTextBox.Enabled = false;
                houseNumberTextBox.Text = "";
                apartmentNumberTextBox.Enabled = false;
                apartmentNumberTextBox.Text = "";
            }
            else
            {
                houseNumberTextBox.Enabled = true;
                apartmentNumberTextBox.Enabled = true;
            }
        }

        private void regionsComboBox_Enter(object sender, EventArgs e)
        {
            regionsComboBox.DroppedDown = true;
        }

        private void citiesComboBox_Enter(object sender, EventArgs e)
        {
            citiesComboBox.DroppedDown = true;
        }

        private void streetsComboBox_Enter(object sender, EventArgs e)
        {
            streetsComboBox.DroppedDown = true;
        }
        #endregion

        #region accessTab

        private void addAdminButton_Click(object sender, EventArgs e)
        {
            var newAccountDialog = new CreateNewAccountForm();
            newAccountDialog.ShowDialog();
            getAllUsersWithRolesTableAdapter.Fill(phoneDictionaryDataSet.GetAllUsersWithRoles);
        }

        private void deleteUserButton_Click(object sender, EventArgs e)
        {
            try
            {
                var row = (DataRowView)getAllUsersWithRolesBindingSource.Current;
                var username = row[usernameDataGridViewTextBoxColumn.DataPropertyName].ToString();

                switch (MessageBox.Show($"Удалить пользователя {username}?", "Удаление", MessageBoxButtons.OKCancel))
                {
                    case DialogResult.OK:                       
                        _accountManager.DeleteUser(username);
                        break;
                }
                getAllUsersWithRolesTableAdapter.Fill(phoneDictionaryDataSet.GetAllUsersWithRoles);

            }
            catch
            {
                MessageBox.Show("Ошибка при удалении пользователя");
            }
            
        }
        
        /// <summary>
        /// Activates delete button on cell selected 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void usersDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (usersDataGridView.SelectedRows.Count > 0)
            {
                deleteUserButton.Enabled = true;
            }
            else
            {
                deleteUserButton.Enabled = false;
            }
        }

        private void usersDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                var clickedColumnName = usersDataGridView.Columns[e.ColumnIndex].DataPropertyName;
                if (clickedColumnName == AuthService.ADMIN_ROLE ||
                    clickedColumnName == AuthService.REDACTOR_ROLE)
                {
                    var row = (DataRowView)getAllUsersWithRolesBindingSource.Current;
                    var username = row[usernameDataGridViewTextBoxColumn.DataPropertyName].ToString();
                    var currentState = (int)usersDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;

                    switch (currentState)
                    {
                        case 0:
                            _accountManager.AddRole(username, clickedColumnName);
                            
                            break;
                        case 1:
                            _accountManager.RemoveRole(username, clickedColumnName);
                            break;
                    }
                }
                
            }
            catch
            {
                MessageBox.Show("Ошибка при изменении роли");
            }

            // Сохраняет выборку i-той строки (по умолчанию сбивается на 0) 
            getAllUsersWithRolesTableAdapter.Fill(phoneDictionaryDataSet.GetAllUsersWithRoles);
            usersDataGridView.Rows[e.RowIndex].Selected = true;
        }





#endregion

        #region regionsTab
        /// <summary>
        /// Управляет поведением кнопки Удалить в соответсвии с наличием выбранного региона
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void regionsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (regionsListBox.SelectedItems.Count > 0)
            {
                deleteRegionButton.Enabled = true;
            }
            else
            {
                deleteRegionButton.Enabled = false;
            }
        }

        private void addRegionButton_Click(object sender, EventArgs e)
        {
            var addRegionDialog = new CreateNewRegionForm();
            var result = addRegionDialog.ShowDialog();

            switch (result)
            {
                case DialogResult.OK:
                    FetchRegionsAndSetToEmpty();
                    break;
            }
        }

        private void deleteRegionButton_Click(object sender, EventArgs e)
        {
            var selectedRegion = ((DataRowView)regionsListBox.SelectedItem).Row["Region"] as string;

            if (MessageBox.Show(
                    @"Вы уверены, что хотите удалить регион?",
                    "Удаление региона",
                    MessageBoxButtons.OKCancel)
                    == DialogResult.OK)
            {
                try
                {
                    queriesTableAdapter.RemoveRegion(selectedRegion);
                    FetchRegions();
                }
                catch
                {
                    MessageBox.Show("Ошибка при удалении региона");
                }

                // Обновление результатов поиска, чтобы не отображать несуществующий регион
                FetchRegionsAndSetToEmpty();
                FetchPersonsByTextBoxValues();
            }           
        }

        private void editRegionButton_Click(object sender, EventArgs e)
        {
            var editRegionDialog = new EditRegionForm(((DataRowView)regionsListBox.SelectedItem).Row["Region"] as string);

            if (editRegionDialog.ShowDialog() == DialogResult.OK)
            {
                FetchRegionsAndSetToEmpty();
                FetchPersonsByTextBoxValues();
            }
            
        }

        private void serachRegionTextBox_TextChanged(object sender, EventArgs e)
        {
            manageRegionsBindingSource.Filter = $"Region LIKE '{serachRegionTextBox.Text}%'";
        }

        #endregion

        #region citiesTab
        private void searchCityTextBox_TextChanged(object sender, EventArgs e)
        {
            manageCitiesBindingSource.Filter = $"City LIKE '{searchCityTextBox.Text}%'";
        }

        private void addCityButton_Click(object sender, EventArgs e)
        {
            var addCityDialog = new CreateNewCityForm();
            if (addCityDialog.ShowDialog() == DialogResult.OK)
            {
                FetchCities();
            }
        }

        private void editCityButton_Click(object sender, EventArgs e)
        {
            var cityName = ((DataRowView)manageCitiesBindingSource.Current).Row["City"].ToString();
            var editCityDialog = new EditCityForm(cityName);

            if (editCityDialog.ShowDialog() == DialogResult.OK)
            {
                FetchCities();
                FetchPersonsByTextBoxValues();
            }
        }

        /// <summary>
        /// Задает логику кнопок Редактировать и Удалить во вкладке Города
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void citiesListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (citiesListBox.SelectedIndex >= 0)
            {
                editCityButton.Enabled = true;
                deleteCityButton.Enabled = true;
            }
            else
            {
                editCityButton.Enabled = false;
                deleteCityButton.Enabled = false;
            }
        }

        private void deleteCityButton_Click(object sender, EventArgs e)
        {
            var cityName = ((DataRowView)manageCitiesBindingSource.Current).Row["City"].ToString();
            if (MessageBox.Show("Вы уверены, что хотите удалить город?", "Удаление города", MessageBoxButtons.OKCancel)
                == DialogResult.OK)
            {
                try
                {
                    queriesTableAdapter.DeleteCity(cityName);
                }
                catch
                {
                    MessageBox.Show("Ошибка при удалении города");
                }
                FetchPersonsByTextBoxValues();
                FetchCities();
            }
        }
        #endregion

        #region streetsTab
        private void streetsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (regionsListBox.SelectedIndex >= 0)
            {
                editSteetButton.Enabled = true;
                deleteStreetButton.Enabled = true;
            }
            else
            {
                editSteetButton.Enabled = true;
                deleteStreetButton.Enabled = true;
            }
        }

        private void addStreetButton_Click(object sender, EventArgs e)
        {
            var addStreetDialog = new CreateNewStreetForm();
            if(addStreetDialog.ShowDialog() == DialogResult.OK)
            {
                FetchStreets();
            }
        }

        private void editSteetButton_Click(object sender, EventArgs e)
        {
            var streetName = ((DataRowView)manageStreetsBindingSource.Current).Row["Street"].ToString();
            var editStreetDialog = new EditStreetForm(streetName);

            if (editStreetDialog.ShowDialog() == DialogResult.OK)
            {
                FetchStreets();
                FetchPersonsByTextBoxValues();
            }
        }

        private void deleteStreetButton_Click(object sender, EventArgs e)
        {
            var streetName = ((DataRowView)manageStreetsBindingSource.Current).Row["Street"].ToString();
            if (MessageBox.Show("Вы уверены, что хотите удалить улицу?", "Удаление улицы", MessageBoxButtons.OKCancel)
                == DialogResult.OK)
            {
                try
                {
                    queriesTableAdapter.DeleteStreet(streetName);                   
                }
                catch
                {
                    MessageBox.Show("Ошибка при удалении улицы");
                }
                FetchStreets();
                FetchPersonsByTextBoxValues();
            }
        }
        private void searchStreetTextBox_TextChanged(object sender, EventArgs e)
        {
            manageStreetsBindingSource.Filter = $"Street LIKE '{searchStreetTextBox.Text}%'";
        }


        #endregion

        
    }
}
