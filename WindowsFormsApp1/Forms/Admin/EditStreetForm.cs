﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters;

namespace WindowsFormsApp1.Forms.Admin
{
    public partial class EditStreetForm : Form
    {
        public QueriesTableAdapter queriesTableAdapter { get; set; } = new QueriesTableAdapter();
        private string _oldStreetName;

        public EditStreetForm(string oldStreetName)
        {
            InitializeComponent();
            _oldStreetName = oldStreetName;
            streetNameTextBox.Text = oldStreetName;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                queriesTableAdapter.UpdateStreet(_oldStreetName, streetNameTextBox.Text);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch
            {
                MessageBox.Show("Ошибка при редактировании улицы");
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
