﻿namespace WindowsFormsApp1
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.secondName = new System.Windows.Forms.Label();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.patronymicTextBox = new System.Windows.Forms.TextBox();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.streetsComboBox = new System.Windows.Forms.ComboBox();
            this.streetsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.phoneDictionaryDataSet = new WindowsFormsApp1.PhoneDictionaryDataSet();
            this.citiesComboBox = new System.Windows.Forms.ComboBox();
            this.citiesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.clearFieldsButton = new System.Windows.Forms.Button();
            this.deletePersonButton = new System.Windows.Forms.Button();
            this.saveChangesButton = new System.Windows.Forms.Button();
            this.deletePhoneNumberButton = new System.Windows.Forms.Button();
            this.changePhoneNumberButton = new System.Windows.Forms.Button();
            this.phoneNumbersListBox = new System.Windows.Forms.ListBox();
            this.addNumberButton = new System.Windows.Forms.Button();
            this.phoneMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.regionsComboBox = new System.Windows.Forms.ComboBox();
            this.regionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.apartmentNumberTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.houseNumberTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.searchButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.birthdayDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewFirstNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewLastNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewPatronymicColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewBirthdayColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewRegionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridStreetColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewHouseNumberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewApartmentNumberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewNumbersColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.getPersonsByParamsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabControl = new System.Windows.Forms.TabControl();
            this.personsPage = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.regionsTabPage = new System.Windows.Forms.TabPage();
            this.serachRegionTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.editRegionButton = new System.Windows.Forms.Button();
            this.deleteRegionButton = new System.Windows.Forms.Button();
            this.addRegionButton = new System.Windows.Forms.Button();
            this.regionsListBox = new System.Windows.Forms.ListBox();
            this.manageRegionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.citiesPage = new System.Windows.Forms.TabPage();
            this.searchCityTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.editCityButton = new System.Windows.Forms.Button();
            this.deleteCityButton = new System.Windows.Forms.Button();
            this.addCityButton = new System.Windows.Forms.Button();
            this.citiesListBox = new System.Windows.Forms.ListBox();
            this.manageCitiesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.streetsPage = new System.Windows.Forms.TabPage();
            this.searchStreetTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.editSteetButton = new System.Windows.Forms.Button();
            this.deleteStreetButton = new System.Windows.Forms.Button();
            this.addStreetButton = new System.Windows.Forms.Button();
            this.streetsListBox = new System.Windows.Forms.ListBox();
            this.manageStreetsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.accessPage = new System.Windows.Forms.TabPage();
            this.usersDataGridView = new System.Windows.Forms.DataGridView();
            this.usernameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adminDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.redactorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.getAllUsersWithRolesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.deleteUserButton = new System.Windows.Forms.Button();
            this.addAdminButton = new System.Windows.Forms.Button();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.userToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backToMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getPersonsByParamsTableAdapter = new WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters.GetPersonsByParamsTableAdapter();
            this.getAllUsersWithRolesTableAdapter = new WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters.GetAllUsersWithRolesTableAdapter();
            this.regionsTableAdapter = new WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters.RegionsTableAdapter();
            this.citiesTableAdapter = new WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters.CitiesTableAdapter();
            this.streetsTableAdapter = new WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters.StreetsTableAdapter();
            this.getCitiesByRegionTableAdapter = new WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters.GetCitiesByRegionTableAdapter();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.streetsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phoneDictionaryDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.citiesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getPersonsByParamsBindingSource)).BeginInit();
            this.tabControl.SuspendLayout();
            this.personsPage.SuspendLayout();
            this.panel1.SuspendLayout();
            this.regionsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.manageRegionsBindingSource)).BeginInit();
            this.citiesPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.manageCitiesBindingSource)).BeginInit();
            this.streetsPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.manageStreetsBindingSource)).BeginInit();
            this.accessPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usersDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getAllUsersWithRolesBindingSource)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // secondName
            // 
            this.secondName.AutoSize = true;
            this.secondName.Location = new System.Drawing.Point(13, 28);
            this.secondName.Name = "secondName";
            this.secondName.Size = new System.Drawing.Size(60, 13);
            this.secondName.TabIndex = 0;
            this.secondName.Text = "Фамилия*";
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.Location = new System.Drawing.Point(105, 24);
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(182, 20);
            this.lastNameTextBox.TabIndex = 0;
            // 
            // patronymicTextBox
            // 
            this.patronymicTextBox.Location = new System.Drawing.Point(105, 76);
            this.patronymicTextBox.Name = "patronymicTextBox";
            this.patronymicTextBox.Size = new System.Drawing.Size(182, 20);
            this.patronymicTextBox.TabIndex = 3;
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Location = new System.Drawing.Point(105, 50);
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(182, 20);
            this.firstNameTextBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Имя*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Отчество*";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.streetsComboBox);
            this.groupBox1.Controls.Add(this.citiesComboBox);
            this.groupBox1.Controls.Add(this.clearFieldsButton);
            this.groupBox1.Controls.Add(this.deletePersonButton);
            this.groupBox1.Controls.Add(this.saveChangesButton);
            this.groupBox1.Controls.Add(this.deletePhoneNumberButton);
            this.groupBox1.Controls.Add(this.changePhoneNumberButton);
            this.groupBox1.Controls.Add(this.phoneNumbersListBox);
            this.groupBox1.Controls.Add(this.addNumberButton);
            this.groupBox1.Controls.Add(this.phoneMaskedTextBox);
            this.groupBox1.Controls.Add(this.addButton);
            this.groupBox1.Controls.Add(this.regionsComboBox);
            this.groupBox1.Controls.Add(this.apartmentNumberTextBox);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.houseNumberTextBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.searchButton);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.birthdayDateTimePicker);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lastNameTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.secondName);
            this.groupBox1.Controls.Add(this.patronymicTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.firstNameTextBox);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox1.Size = new System.Drawing.Size(824, 180);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Персональные данные";
            // 
            // streetsComboBox
            // 
            this.streetsComboBox.DataSource = this.streetsBindingSource;
            this.streetsComboBox.DisplayMember = "Street";
            this.streetsComboBox.FormattingEnabled = true;
            this.streetsComboBox.Location = new System.Drawing.Point(374, 76);
            this.streetsComboBox.Name = "streetsComboBox";
            this.streetsComboBox.Size = new System.Drawing.Size(145, 21);
            this.streetsComboBox.TabIndex = 7;
            this.streetsComboBox.TextChanged += new System.EventHandler(this.streetsComboBox_TextChanged);
            this.streetsComboBox.Enter += new System.EventHandler(this.streetsComboBox_Enter);
            // 
            // streetsBindingSource
            // 
            this.streetsBindingSource.DataMember = "Streets";
            this.streetsBindingSource.DataSource = this.phoneDictionaryDataSet;
            this.streetsBindingSource.Sort = "Street asc";
            // 
            // phoneDictionaryDataSet
            // 
            this.phoneDictionaryDataSet.DataSetName = "PhoneDictionaryDataSet";
            this.phoneDictionaryDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // citiesComboBox
            // 
            this.citiesComboBox.DataSource = this.citiesBindingSource;
            this.citiesComboBox.DisplayMember = "City";
            this.citiesComboBox.FormattingEnabled = true;
            this.citiesComboBox.Location = new System.Drawing.Point(374, 50);
            this.citiesComboBox.Name = "citiesComboBox";
            this.citiesComboBox.Size = new System.Drawing.Size(145, 21);
            this.citiesComboBox.TabIndex = 6;
            this.citiesComboBox.Enter += new System.EventHandler(this.citiesComboBox_Enter);
            // 
            // citiesBindingSource
            // 
            this.citiesBindingSource.DataMember = "Cities";
            this.citiesBindingSource.DataSource = this.phoneDictionaryDataSet;
            this.citiesBindingSource.Sort = "City asc";
            // 
            // clearFieldsButton
            // 
            this.clearFieldsButton.Location = new System.Drawing.Point(97, 145);
            this.clearFieldsButton.Name = "clearFieldsButton";
            this.clearFieldsButton.Size = new System.Drawing.Size(75, 23);
            this.clearFieldsButton.TabIndex = 14;
            this.clearFieldsButton.Text = "Очистить";
            this.clearFieldsButton.UseVisualStyleBackColor = true;
            this.clearFieldsButton.Click += new System.EventHandler(this.clearFieldsButton_Click);
            // 
            // deletePersonButton
            // 
            this.deletePersonButton.Enabled = false;
            this.deletePersonButton.Location = new System.Drawing.Point(454, 145);
            this.deletePersonButton.Name = "deletePersonButton";
            this.deletePersonButton.Size = new System.Drawing.Size(108, 23);
            this.deletePersonButton.TabIndex = 28;
            this.deletePersonButton.TabStop = false;
            this.deletePersonButton.Text = "Удалить";
            this.deletePersonButton.UseVisualStyleBackColor = true;
            this.deletePersonButton.Click += new System.EventHandler(this.deletePersonButton_Click);
            // 
            // saveChangesButton
            // 
            this.saveChangesButton.Enabled = false;
            this.saveChangesButton.Location = new System.Drawing.Point(568, 145);
            this.saveChangesButton.Name = "saveChangesButton";
            this.saveChangesButton.Size = new System.Drawing.Size(127, 23);
            this.saveChangesButton.TabIndex = 27;
            this.saveChangesButton.TabStop = false;
            this.saveChangesButton.Text = "Сохранить изменения";
            this.saveChangesButton.UseVisualStyleBackColor = true;
            this.saveChangesButton.Click += new System.EventHandler(this.saveChangesButton_Click);
            // 
            // deletePhoneNumberButton
            // 
            this.deletePhoneNumberButton.Enabled = false;
            this.deletePhoneNumberButton.Location = new System.Drawing.Point(712, 99);
            this.deletePhoneNumberButton.Name = "deletePhoneNumberButton";
            this.deletePhoneNumberButton.Size = new System.Drawing.Size(93, 23);
            this.deletePhoneNumberButton.TabIndex = 26;
            this.deletePhoneNumberButton.TabStop = false;
            this.deletePhoneNumberButton.Text = "Удалить номер";
            this.deletePhoneNumberButton.UseVisualStyleBackColor = true;
            this.deletePhoneNumberButton.Click += new System.EventHandler(this.deletePhoneNumberButton_Click);
            // 
            // changePhoneNumberButton
            // 
            this.changePhoneNumberButton.Enabled = false;
            this.changePhoneNumberButton.Location = new System.Drawing.Point(568, 99);
            this.changePhoneNumberButton.Name = "changePhoneNumberButton";
            this.changePhoneNumberButton.Size = new System.Drawing.Size(93, 23);
            this.changePhoneNumberButton.TabIndex = 25;
            this.changePhoneNumberButton.TabStop = false;
            this.changePhoneNumberButton.Text = "Изменить";
            this.changePhoneNumberButton.UseVisualStyleBackColor = true;
            this.changePhoneNumberButton.Click += new System.EventHandler(this.changePhoneNumberButton_Click);
            // 
            // phoneNumbersListBox
            // 
            this.phoneNumbersListBox.FormattingEnabled = true;
            this.phoneNumbersListBox.Location = new System.Drawing.Point(568, 50);
            this.phoneNumbersListBox.Name = "phoneNumbersListBox";
            this.phoneNumbersListBox.Size = new System.Drawing.Size(237, 43);
            this.phoneNumbersListBox.TabIndex = 24;
            this.phoneNumbersListBox.TabStop = false;
            this.phoneNumbersListBox.SelectedIndexChanged += new System.EventHandler(this.phoneNumbersListBox_SelectedIndexChanged);
            // 
            // addNumberButton
            // 
            this.addNumberButton.Location = new System.Drawing.Point(730, 21);
            this.addNumberButton.Name = "addNumberButton";
            this.addNumberButton.Size = new System.Drawing.Size(75, 23);
            this.addNumberButton.TabIndex = 11;
            this.addNumberButton.Text = "Добавить";
            this.addNumberButton.UseVisualStyleBackColor = true;
            this.addNumberButton.Click += new System.EventHandler(this.addNumberButton_Click);
            // 
            // phoneMaskedTextBox
            // 
            this.phoneMaskedTextBox.Location = new System.Drawing.Point(568, 22);
            this.phoneMaskedTextBox.Mask = "(999) 000-0000";
            this.phoneMaskedTextBox.Name = "phoneMaskedTextBox";
            this.phoneMaskedTextBox.Size = new System.Drawing.Size(156, 20);
            this.phoneMaskedTextBox.TabIndex = 10;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(701, 145);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(104, 23);
            this.addButton.TabIndex = 13;
            this.addButton.Text = "Добавить запись";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // regionsComboBox
            // 
            this.regionsComboBox.DataSource = this.regionsBindingSource;
            this.regionsComboBox.DisplayMember = "Region";
            this.regionsComboBox.FormattingEnabled = true;
            this.regionsComboBox.Location = new System.Drawing.Point(374, 22);
            this.regionsComboBox.Name = "regionsComboBox";
            this.regionsComboBox.Size = new System.Drawing.Size(145, 21);
            this.regionsComboBox.TabIndex = 5;
            this.regionsComboBox.Enter += new System.EventHandler(this.regionsComboBox_Enter);
            // 
            // regionsBindingSource
            // 
            this.regionsBindingSource.DataMember = "Regions";
            this.regionsBindingSource.DataSource = this.phoneDictionaryDataSet;
            this.regionsBindingSource.Filter = "";
            this.regionsBindingSource.Sort = "Region asc";
            // 
            // apartmentNumberTextBox
            // 
            this.apartmentNumberTextBox.Enabled = false;
            this.apartmentNumberTextBox.Location = new System.Drawing.Point(479, 102);
            this.apartmentNumberTextBox.Name = "apartmentNumberTextBox";
            this.apartmentNumberTextBox.Size = new System.Drawing.Size(40, 20);
            this.apartmentNumberTextBox.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(451, 106);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "кв.";
            // 
            // houseNumberTextBox
            // 
            this.houseNumberTextBox.Enabled = false;
            this.houseNumberTextBox.Location = new System.Drawing.Point(400, 102);
            this.houseNumberTextBox.Name = "houseNumberTextBox";
            this.houseNumberTextBox.Size = new System.Drawing.Size(40, 20);
            this.houseNumberTextBox.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(324, 106);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Номер дома";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(324, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Улица";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(324, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Город";
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(16, 145);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 23);
            this.searchButton.TabIndex = 12;
            this.searchButton.Text = "Поиск";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(324, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Регион";
            // 
            // birthdayDateTimePicker
            // 
            this.birthdayDateTimePicker.CustomFormat = "dd.mm.yyyy";
            this.birthdayDateTimePicker.Location = new System.Drawing.Point(105, 102);
            this.birthdayDateTimePicker.Name = "birthdayDateTimePicker";
            this.birthdayDateTimePicker.ShowCheckBox = true;
            this.birthdayDateTimePicker.ShowUpDown = true;
            this.birthdayDateTimePicker.Size = new System.Drawing.Size(182, 20);
            this.birthdayDateTimePicker.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Дата рождения";
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AutoGenerateColumns = false;
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.dataGridViewFirstNameColumn,
            this.dataGridViewLastNameColumn,
            this.dataGridViewPatronymicColumn,
            this.dataGridViewBirthdayColumn,
            this.dataGridViewRegionColumn,
            this.dataGridViewCityColumn,
            this.dataGridStreetColumn,
            this.dataGridViewHouseNumberColumn,
            this.dataGridViewApartmentNumberColumn,
            this.dataGridViewNumbersColumn});
            this.dataGridView.DataSource = this.getPersonsByParamsBindingSource;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(0, 0);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersWidth = 4;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(824, 343);
            this.dataGridView.TabIndex = 7;
            this.dataGridView.TabStop = false;
            this.dataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // dataGridViewFirstNameColumn
            // 
            this.dataGridViewFirstNameColumn.DataPropertyName = "Имя";
            this.dataGridViewFirstNameColumn.HeaderText = "Имя";
            this.dataGridViewFirstNameColumn.Name = "dataGridViewFirstNameColumn";
            this.dataGridViewFirstNameColumn.ReadOnly = true;
            // 
            // dataGridViewLastNameColumn
            // 
            this.dataGridViewLastNameColumn.DataPropertyName = "Фамилия";
            this.dataGridViewLastNameColumn.HeaderText = "Фамилия";
            this.dataGridViewLastNameColumn.Name = "dataGridViewLastNameColumn";
            this.dataGridViewLastNameColumn.ReadOnly = true;
            // 
            // dataGridViewPatronymicColumn
            // 
            this.dataGridViewPatronymicColumn.DataPropertyName = "Отчество";
            this.dataGridViewPatronymicColumn.HeaderText = "Отчество";
            this.dataGridViewPatronymicColumn.Name = "dataGridViewPatronymicColumn";
            this.dataGridViewPatronymicColumn.ReadOnly = true;
            // 
            // dataGridViewBirthdayColumn
            // 
            this.dataGridViewBirthdayColumn.DataPropertyName = "День Рождения";
            this.dataGridViewBirthdayColumn.HeaderText = "День Рождения";
            this.dataGridViewBirthdayColumn.Name = "dataGridViewBirthdayColumn";
            this.dataGridViewBirthdayColumn.ReadOnly = true;
            // 
            // dataGridViewRegionColumn
            // 
            this.dataGridViewRegionColumn.DataPropertyName = "Регион";
            this.dataGridViewRegionColumn.HeaderText = "Регион";
            this.dataGridViewRegionColumn.Name = "dataGridViewRegionColumn";
            this.dataGridViewRegionColumn.ReadOnly = true;
            // 
            // dataGridViewCityColumn
            // 
            this.dataGridViewCityColumn.DataPropertyName = "Город";
            this.dataGridViewCityColumn.HeaderText = "Город";
            this.dataGridViewCityColumn.Name = "dataGridViewCityColumn";
            this.dataGridViewCityColumn.ReadOnly = true;
            // 
            // dataGridStreetColumn
            // 
            this.dataGridStreetColumn.DataPropertyName = "Улица";
            this.dataGridStreetColumn.HeaderText = "Улица";
            this.dataGridStreetColumn.Name = "dataGridStreetColumn";
            this.dataGridStreetColumn.ReadOnly = true;
            // 
            // dataGridViewHouseNumberColumn
            // 
            this.dataGridViewHouseNumberColumn.DataPropertyName = "HouseNumber";
            this.dataGridViewHouseNumberColumn.HeaderText = "Номер дома";
            this.dataGridViewHouseNumberColumn.Name = "dataGridViewHouseNumberColumn";
            this.dataGridViewHouseNumberColumn.ReadOnly = true;
            // 
            // dataGridViewApartmentNumberColumn
            // 
            this.dataGridViewApartmentNumberColumn.DataPropertyName = "ApartmentNumber";
            this.dataGridViewApartmentNumberColumn.HeaderText = "Квартира";
            this.dataGridViewApartmentNumberColumn.Name = "dataGridViewApartmentNumberColumn";
            this.dataGridViewApartmentNumberColumn.ReadOnly = true;
            // 
            // dataGridViewNumbersColumn
            // 
            this.dataGridViewNumbersColumn.DataPropertyName = "Номера";
            this.dataGridViewNumbersColumn.HeaderText = "Номера";
            this.dataGridViewNumbersColumn.Name = "dataGridViewNumbersColumn";
            this.dataGridViewNumbersColumn.ReadOnly = true;
            // 
            // getPersonsByParamsBindingSource
            // 
            this.getPersonsByParamsBindingSource.DataMember = "GetPersonsByParams";
            this.getPersonsByParamsBindingSource.DataSource = this.phoneDictionaryDataSet;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.personsPage);
            this.tabControl.Controls.Add(this.regionsTabPage);
            this.tabControl.Controls.Add(this.citiesPage);
            this.tabControl.Controls.Add(this.streetsPage);
            this.tabControl.Controls.Add(this.accessPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 24);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(838, 555);
            this.tabControl.TabIndex = 8;
            this.tabControl.TabStop = false;
            // 
            // personsPage
            // 
            this.personsPage.Controls.Add(this.panel1);
            this.personsPage.Controls.Add(this.groupBox1);
            this.personsPage.Location = new System.Drawing.Point(4, 22);
            this.personsPage.Name = "personsPage";
            this.personsPage.Padding = new System.Windows.Forms.Padding(3);
            this.personsPage.Size = new System.Drawing.Size(830, 529);
            this.personsPage.TabIndex = 0;
            this.personsPage.Text = "Менеджмент справочника";
            this.personsPage.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dataGridView);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 183);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(824, 343);
            this.panel1.TabIndex = 9;
            // 
            // regionsTabPage
            // 
            this.regionsTabPage.Controls.Add(this.serachRegionTextBox);
            this.regionsTabPage.Controls.Add(this.label13);
            this.regionsTabPage.Controls.Add(this.editRegionButton);
            this.regionsTabPage.Controls.Add(this.deleteRegionButton);
            this.regionsTabPage.Controls.Add(this.addRegionButton);
            this.regionsTabPage.Controls.Add(this.regionsListBox);
            this.regionsTabPage.Location = new System.Drawing.Point(4, 22);
            this.regionsTabPage.Name = "regionsTabPage";
            this.regionsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.regionsTabPage.Size = new System.Drawing.Size(830, 529);
            this.regionsTabPage.TabIndex = 2;
            this.regionsTabPage.Text = "Регионы";
            this.regionsTabPage.UseVisualStyleBackColor = true;
            // 
            // serachRegionTextBox
            // 
            this.serachRegionTextBox.Location = new System.Drawing.Point(8, 34);
            this.serachRegionTextBox.Name = "serachRegionTextBox";
            this.serachRegionTextBox.Size = new System.Drawing.Size(620, 20);
            this.serachRegionTextBox.TabIndex = 7;
            this.serachRegionTextBox.TextChanged += new System.EventHandler(this.serachRegionTextBox_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(144, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Начните вводить название";
            // 
            // editRegionButton
            // 
            this.editRegionButton.Location = new System.Drawing.Point(643, 63);
            this.editRegionButton.Name = "editRegionButton";
            this.editRegionButton.Size = new System.Drawing.Size(179, 23);
            this.editRegionButton.TabIndex = 5;
            this.editRegionButton.Text = "Редактировать";
            this.editRegionButton.UseVisualStyleBackColor = true;
            this.editRegionButton.Click += new System.EventHandler(this.editRegionButton_Click);
            // 
            // deleteRegionButton
            // 
            this.deleteRegionButton.Enabled = false;
            this.deleteRegionButton.Location = new System.Drawing.Point(643, 92);
            this.deleteRegionButton.Name = "deleteRegionButton";
            this.deleteRegionButton.Size = new System.Drawing.Size(179, 23);
            this.deleteRegionButton.TabIndex = 4;
            this.deleteRegionButton.Text = "Удалить";
            this.deleteRegionButton.UseVisualStyleBackColor = true;
            this.deleteRegionButton.Click += new System.EventHandler(this.deleteRegionButton_Click);
            // 
            // addRegionButton
            // 
            this.addRegionButton.Location = new System.Drawing.Point(643, 34);
            this.addRegionButton.Name = "addRegionButton";
            this.addRegionButton.Size = new System.Drawing.Size(179, 23);
            this.addRegionButton.TabIndex = 3;
            this.addRegionButton.Text = "Добавить";
            this.addRegionButton.UseVisualStyleBackColor = true;
            this.addRegionButton.Click += new System.EventHandler(this.addRegionButton_Click);
            // 
            // regionsListBox
            // 
            this.regionsListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.regionsListBox.DataSource = this.manageRegionsBindingSource;
            this.regionsListBox.DisplayMember = "Region";
            this.regionsListBox.FormattingEnabled = true;
            this.regionsListBox.Location = new System.Drawing.Point(8, 70);
            this.regionsListBox.Name = "regionsListBox";
            this.regionsListBox.Size = new System.Drawing.Size(620, 433);
            this.regionsListBox.TabIndex = 0;
            this.regionsListBox.SelectedIndexChanged += new System.EventHandler(this.regionsListBox_SelectedIndexChanged);
            // 
            // manageRegionsBindingSource
            // 
            this.manageRegionsBindingSource.DataMember = "Regions";
            this.manageRegionsBindingSource.DataSource = this.phoneDictionaryDataSet;
            this.manageRegionsBindingSource.Sort = "Region asc";
            // 
            // citiesPage
            // 
            this.citiesPage.Controls.Add(this.searchCityTextBox);
            this.citiesPage.Controls.Add(this.label12);
            this.citiesPage.Controls.Add(this.editCityButton);
            this.citiesPage.Controls.Add(this.deleteCityButton);
            this.citiesPage.Controls.Add(this.addCityButton);
            this.citiesPage.Controls.Add(this.citiesListBox);
            this.citiesPage.Location = new System.Drawing.Point(4, 22);
            this.citiesPage.Name = "citiesPage";
            this.citiesPage.Size = new System.Drawing.Size(830, 529);
            this.citiesPage.TabIndex = 3;
            this.citiesPage.Text = "Города";
            this.citiesPage.UseVisualStyleBackColor = true;
            // 
            // searchCityTextBox
            // 
            this.searchCityTextBox.Location = new System.Drawing.Point(8, 34);
            this.searchCityTextBox.Name = "searchCityTextBox";
            this.searchCityTextBox.Size = new System.Drawing.Size(620, 20);
            this.searchCityTextBox.TabIndex = 13;
            this.searchCityTextBox.TextChanged += new System.EventHandler(this.searchCityTextBox_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(144, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Начните вводить название";
            // 
            // editCityButton
            // 
            this.editCityButton.Enabled = false;
            this.editCityButton.Location = new System.Drawing.Point(643, 63);
            this.editCityButton.Name = "editCityButton";
            this.editCityButton.Size = new System.Drawing.Size(179, 23);
            this.editCityButton.TabIndex = 11;
            this.editCityButton.Text = "Редактировать";
            this.editCityButton.UseVisualStyleBackColor = true;
            this.editCityButton.Click += new System.EventHandler(this.editCityButton_Click);
            // 
            // deleteCityButton
            // 
            this.deleteCityButton.Enabled = false;
            this.deleteCityButton.Location = new System.Drawing.Point(643, 92);
            this.deleteCityButton.Name = "deleteCityButton";
            this.deleteCityButton.Size = new System.Drawing.Size(179, 23);
            this.deleteCityButton.TabIndex = 10;
            this.deleteCityButton.Text = "Удалить";
            this.deleteCityButton.UseVisualStyleBackColor = true;
            this.deleteCityButton.Click += new System.EventHandler(this.deleteCityButton_Click);
            // 
            // addCityButton
            // 
            this.addCityButton.Location = new System.Drawing.Point(643, 34);
            this.addCityButton.Name = "addCityButton";
            this.addCityButton.Size = new System.Drawing.Size(179, 23);
            this.addCityButton.TabIndex = 9;
            this.addCityButton.Text = "Добавить";
            this.addCityButton.UseVisualStyleBackColor = true;
            this.addCityButton.Click += new System.EventHandler(this.addCityButton_Click);
            // 
            // citiesListBox
            // 
            this.citiesListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.citiesListBox.DataSource = this.manageCitiesBindingSource;
            this.citiesListBox.DisplayMember = "City";
            this.citiesListBox.FormattingEnabled = true;
            this.citiesListBox.Location = new System.Drawing.Point(8, 70);
            this.citiesListBox.Name = "citiesListBox";
            this.citiesListBox.Size = new System.Drawing.Size(620, 433);
            this.citiesListBox.TabIndex = 8;
            this.citiesListBox.SelectedIndexChanged += new System.EventHandler(this.citiesListBox_SelectedIndexChanged);
            // 
            // manageCitiesBindingSource
            // 
            this.manageCitiesBindingSource.DataMember = "Cities";
            this.manageCitiesBindingSource.DataSource = this.phoneDictionaryDataSet;
            // 
            // streetsPage
            // 
            this.streetsPage.Controls.Add(this.searchStreetTextBox);
            this.streetsPage.Controls.Add(this.label14);
            this.streetsPage.Controls.Add(this.editSteetButton);
            this.streetsPage.Controls.Add(this.deleteStreetButton);
            this.streetsPage.Controls.Add(this.addStreetButton);
            this.streetsPage.Controls.Add(this.streetsListBox);
            this.streetsPage.Location = new System.Drawing.Point(4, 22);
            this.streetsPage.Name = "streetsPage";
            this.streetsPage.Size = new System.Drawing.Size(830, 529);
            this.streetsPage.TabIndex = 4;
            this.streetsPage.Text = "Улицы";
            this.streetsPage.UseVisualStyleBackColor = true;
            // 
            // searchStreetTextBox
            // 
            this.searchStreetTextBox.Location = new System.Drawing.Point(8, 34);
            this.searchStreetTextBox.Name = "searchStreetTextBox";
            this.searchStreetTextBox.Size = new System.Drawing.Size(620, 20);
            this.searchStreetTextBox.TabIndex = 13;
            this.searchStreetTextBox.TextChanged += new System.EventHandler(this.searchStreetTextBox_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(5, 18);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(144, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "Начните вводить название";
            // 
            // editSteetButton
            // 
            this.editSteetButton.Enabled = false;
            this.editSteetButton.Location = new System.Drawing.Point(643, 63);
            this.editSteetButton.Name = "editSteetButton";
            this.editSteetButton.Size = new System.Drawing.Size(179, 23);
            this.editSteetButton.TabIndex = 11;
            this.editSteetButton.Text = "Редактировать";
            this.editSteetButton.UseVisualStyleBackColor = true;
            this.editSteetButton.Click += new System.EventHandler(this.editSteetButton_Click);
            // 
            // deleteStreetButton
            // 
            this.deleteStreetButton.Enabled = false;
            this.deleteStreetButton.Location = new System.Drawing.Point(643, 92);
            this.deleteStreetButton.Name = "deleteStreetButton";
            this.deleteStreetButton.Size = new System.Drawing.Size(179, 23);
            this.deleteStreetButton.TabIndex = 10;
            this.deleteStreetButton.Text = "Удалить";
            this.deleteStreetButton.UseVisualStyleBackColor = true;
            this.deleteStreetButton.Click += new System.EventHandler(this.deleteStreetButton_Click);
            // 
            // addStreetButton
            // 
            this.addStreetButton.Location = new System.Drawing.Point(643, 34);
            this.addStreetButton.Name = "addStreetButton";
            this.addStreetButton.Size = new System.Drawing.Size(179, 23);
            this.addStreetButton.TabIndex = 9;
            this.addStreetButton.Text = "Добавить";
            this.addStreetButton.UseVisualStyleBackColor = true;
            this.addStreetButton.Click += new System.EventHandler(this.addStreetButton_Click);
            // 
            // streetsListBox
            // 
            this.streetsListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.streetsListBox.DataSource = this.manageStreetsBindingSource;
            this.streetsListBox.DisplayMember = "Street";
            this.streetsListBox.FormattingEnabled = true;
            this.streetsListBox.Location = new System.Drawing.Point(8, 70);
            this.streetsListBox.Name = "streetsListBox";
            this.streetsListBox.Size = new System.Drawing.Size(620, 433);
            this.streetsListBox.TabIndex = 8;
            this.streetsListBox.SelectedIndexChanged += new System.EventHandler(this.streetsListBox_SelectedIndexChanged);
            // 
            // manageStreetsBindingSource
            // 
            this.manageStreetsBindingSource.DataMember = "Streets";
            this.manageStreetsBindingSource.DataSource = this.phoneDictionaryDataSet;
            // 
            // accessPage
            // 
            this.accessPage.Controls.Add(this.usersDataGridView);
            this.accessPage.Controls.Add(this.label11);
            this.accessPage.Controls.Add(this.label10);
            this.accessPage.Controls.Add(this.label9);
            this.accessPage.Controls.Add(this.deleteUserButton);
            this.accessPage.Controls.Add(this.addAdminButton);
            this.accessPage.Location = new System.Drawing.Point(4, 22);
            this.accessPage.Name = "accessPage";
            this.accessPage.Padding = new System.Windows.Forms.Padding(3);
            this.accessPage.Size = new System.Drawing.Size(830, 529);
            this.accessPage.TabIndex = 1;
            this.accessPage.Text = "Управление доступом";
            this.accessPage.UseVisualStyleBackColor = true;
            // 
            // usersDataGridView
            // 
            this.usersDataGridView.AllowUserToAddRows = false;
            this.usersDataGridView.AllowUserToDeleteRows = false;
            this.usersDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.usersDataGridView.AutoGenerateColumns = false;
            this.usersDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.usersDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.usersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.usersDataGridView.ColumnHeadersVisible = false;
            this.usersDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.usernameDataGridViewTextBoxColumn,
            this.adminDataGridViewTextBoxColumn,
            this.redactorDataGridViewTextBoxColumn});
            this.usersDataGridView.DataSource = this.getAllUsersWithRolesBindingSource;
            this.usersDataGridView.Location = new System.Drawing.Point(13, 37);
            this.usersDataGridView.MultiSelect = false;
            this.usersDataGridView.Name = "usersDataGridView";
            this.usersDataGridView.ReadOnly = true;
            this.usersDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.usersDataGridView.Size = new System.Drawing.Size(620, 460);
            this.usersDataGridView.TabIndex = 9;
            this.usersDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.usersDataGridView_CellContentClick);
            this.usersDataGridView.SelectionChanged += new System.EventHandler(this.usersDataGridView_SelectionChanged);
            // 
            // usernameDataGridViewTextBoxColumn
            // 
            this.usernameDataGridViewTextBoxColumn.DataPropertyName = "Username";
            this.usernameDataGridViewTextBoxColumn.HeaderText = "Username";
            this.usernameDataGridViewTextBoxColumn.Name = "usernameDataGridViewTextBoxColumn";
            this.usernameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // adminDataGridViewTextBoxColumn
            // 
            this.adminDataGridViewTextBoxColumn.DataPropertyName = "Admin";
            this.adminDataGridViewTextBoxColumn.FalseValue = "0";
            this.adminDataGridViewTextBoxColumn.HeaderText = "Admin";
            this.adminDataGridViewTextBoxColumn.Name = "adminDataGridViewTextBoxColumn";
            this.adminDataGridViewTextBoxColumn.ReadOnly = true;
            this.adminDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.adminDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.adminDataGridViewTextBoxColumn.TrueValue = "1";
            // 
            // redactorDataGridViewTextBoxColumn
            // 
            this.redactorDataGridViewTextBoxColumn.DataPropertyName = "Redactor";
            this.redactorDataGridViewTextBoxColumn.HeaderText = "Redactor";
            this.redactorDataGridViewTextBoxColumn.Name = "redactorDataGridViewTextBoxColumn";
            this.redactorDataGridViewTextBoxColumn.ReadOnly = true;
            this.redactorDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.redactorDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // getAllUsersWithRolesBindingSource
            // 
            this.getAllUsersWithRolesBindingSource.DataMember = "GetAllUsersWithRoles";
            this.getAllUsersWithRolesBindingSource.DataSource = this.phoneDictionaryDataSet;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(442, 18);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(145, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "Управление справочником";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(239, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(138, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Управление уч. записями";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Имя пользователя";
            // 
            // deleteUserButton
            // 
            this.deleteUserButton.Enabled = false;
            this.deleteUserButton.Location = new System.Drawing.Point(647, 66);
            this.deleteUserButton.Name = "deleteUserButton";
            this.deleteUserButton.Size = new System.Drawing.Size(179, 23);
            this.deleteUserButton.TabIndex = 2;
            this.deleteUserButton.Text = "Удалить";
            this.deleteUserButton.UseVisualStyleBackColor = true;
            this.deleteUserButton.Click += new System.EventHandler(this.deleteUserButton_Click);
            // 
            // addAdminButton
            // 
            this.addAdminButton.Location = new System.Drawing.Point(647, 37);
            this.addAdminButton.Name = "addAdminButton";
            this.addAdminButton.Size = new System.Drawing.Size(179, 23);
            this.addAdminButton.TabIndex = 1;
            this.addAdminButton.Text = "Добавить";
            this.addAdminButton.UseVisualStyleBackColor = true;
            this.addAdminButton.Click += new System.EventHandler(this.addAdminButton_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(838, 24);
            this.menuStrip.TabIndex = 9;
            this.menuStrip.Text = "menuStrip1";
            // 
            // userToolStripMenuItem
            // 
            this.userToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.userToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backToMenuToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.userToolStripMenuItem.Name = "userToolStripMenuItem";
            this.userToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.userToolStripMenuItem.Text = "Пользователь";
            // 
            // backToMenuToolStripMenuItem
            // 
            this.backToMenuToolStripMenuItem.Name = "backToMenuToolStripMenuItem";
            this.backToMenuToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.backToMenuToolStripMenuItem.Text = "Выйти обратно в меню";
            this.backToMenuToolStripMenuItem.Click += new System.EventHandler(this.backToMenuToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.logoutToolStripMenuItem.Text = "Выйти из учетной записи";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // getPersonsByParamsTableAdapter
            // 
            this.getPersonsByParamsTableAdapter.ClearBeforeFill = true;
            // 
            // getAllUsersWithRolesTableAdapter
            // 
            this.getAllUsersWithRolesTableAdapter.ClearBeforeFill = true;
            // 
            // regionsTableAdapter
            // 
            this.regionsTableAdapter.ClearBeforeFill = true;
            // 
            // citiesTableAdapter
            // 
            this.citiesTableAdapter.ClearBeforeFill = true;
            // 
            // streetsTableAdapter
            // 
            this.streetsTableAdapter.ClearBeforeFill = true;
            // 
            // getCitiesByRegionTableAdapter
            // 
            this.getCitiesByRegionTableAdapter.ClearBeforeFill = true;
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(838, 579);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "AdminForm";
            this.Text = "Редактор";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AdminForm_FormClosed);
            this.Load += new System.EventHandler(this.AdminForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.streetsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phoneDictionaryDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.citiesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getPersonsByParamsBindingSource)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.personsPage.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.regionsTabPage.ResumeLayout(false);
            this.regionsTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.manageRegionsBindingSource)).EndInit();
            this.citiesPage.ResumeLayout(false);
            this.citiesPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.manageCitiesBindingSource)).EndInit();
            this.streetsPage.ResumeLayout(false);
            this.streetsPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.manageStreetsBindingSource)).EndInit();
            this.accessPage.ResumeLayout(false);
            this.accessPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usersDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getAllUsersWithRolesBindingSource)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label secondName;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.TextBox patronymicTextBox;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker birthdayDateTimePicker;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox houseNumberTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox apartmentNumberTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox regionsComboBox;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage personsPage;
        private System.Windows.Forms.TabPage accessPage;
        private System.Windows.Forms.Button deleteUserButton;
        private System.Windows.Forms.Button addAdminButton;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView usersDataGridView;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.ListBox phoneNumbersListBox;
        private System.Windows.Forms.Button addNumberButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.BindingSource getPersonsByParamsBindingSource;
        private PhoneDictionaryDataSet phoneDictionaryDataSet;
        private PhoneDictionaryDataSetTableAdapters.GetPersonsByParamsTableAdapter getPersonsByParamsTableAdapter;
        private System.Windows.Forms.Button changePhoneNumberButton;
        private System.Windows.Forms.Button deletePhoneNumberButton;
        public System.Windows.Forms.MaskedTextBox phoneMaskedTextBox;
        private System.Windows.Forms.Button saveChangesButton;
        private System.Windows.Forms.Button deletePersonButton;
        private System.Windows.Forms.Button clearFieldsButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewFirstNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewLastNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewPatronymicColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewBirthdayColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewRegionColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewCityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridStreetColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewHouseNumberColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewApartmentNumberColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewNumbersColumn;
        private System.Windows.Forms.BindingSource getAllUsersWithRolesBindingSource;
        private PhoneDictionaryDataSetTableAdapters.GetAllUsersWithRolesTableAdapter getAllUsersWithRolesTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn usernameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn adminDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn redactorDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource regionsBindingSource;
        private PhoneDictionaryDataSetTableAdapters.RegionsTableAdapter regionsTableAdapter;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem userToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backToMenuToolStripMenuItem;
        private System.Windows.Forms.TabPage regionsTabPage;
        private System.Windows.Forms.ListBox regionsListBox;
        private System.Windows.Forms.Button deleteRegionButton;
        private System.Windows.Forms.Button addRegionButton;
        private System.Windows.Forms.Button editRegionButton;
        private System.Windows.Forms.TextBox serachRegionTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.BindingSource manageRegionsBindingSource;
        private System.Windows.Forms.TabPage citiesPage;
        private System.Windows.Forms.TabPage streetsPage;
        private System.Windows.Forms.TextBox searchCityTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button editCityButton;
        private System.Windows.Forms.Button deleteCityButton;
        private System.Windows.Forms.Button addCityButton;
        private System.Windows.Forms.ListBox citiesListBox;
        private System.Windows.Forms.TextBox searchStreetTextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button editSteetButton;
        private System.Windows.Forms.Button deleteStreetButton;
        private System.Windows.Forms.Button addStreetButton;
        private System.Windows.Forms.ListBox streetsListBox;
        private System.Windows.Forms.BindingSource manageCitiesBindingSource;
        private PhoneDictionaryDataSetTableAdapters.CitiesTableAdapter citiesTableAdapter;
        private System.Windows.Forms.BindingSource manageStreetsBindingSource;
        private PhoneDictionaryDataSetTableAdapters.StreetsTableAdapter streetsTableAdapter;
        private PhoneDictionaryDataSetTableAdapters.GetCitiesByRegionTableAdapter getCitiesByRegionTableAdapter;
        private System.Windows.Forms.BindingSource citiesBindingSource;
        private System.Windows.Forms.ComboBox citiesComboBox;
        private System.Windows.Forms.ComboBox streetsComboBox;
        private System.Windows.Forms.BindingSource streetsBindingSource;
    }
}