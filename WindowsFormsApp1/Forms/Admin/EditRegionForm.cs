﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters;

namespace WindowsFormsApp1.Forms.Admin
{
    public partial class EditRegionForm : Form
    {
        public string OriginalName { get; private set; }
        public QueriesTableAdapter queriesTableAdapter { get; set; } = new QueriesTableAdapter();

        public EditRegionForm(string regionToEditName)
        {
            InitializeComponent();
            OriginalName = regionToEditName;
            regionNameTextBox.Text = regionToEditName;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (OriginalName != regionNameTextBox.Text)
                {
                    queriesTableAdapter.ChangeRegionName(OriginalName, regionNameTextBox.Text);
                    DialogResult = DialogResult.OK;
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при переименовывании региона. Убедитесь, что имя региона существует. Также, нельзя переименовать на существующий регион");
            }
            
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
