﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters;

namespace WindowsFormsApp1.Forms.Admin
{
    public partial class CreateNewRegionForm : Form
    {
        public QueriesTableAdapter queriesTableAdapter { get; set; } = new QueriesTableAdapter();
        public CreateNewRegionForm()
        {
            InitializeComponent();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                queriesTableAdapter.AddRegion(regionNameTextBox.Text);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch
            {
                MessageBox.Show("Ошибка при добавлении региона. Возможно, такое название уже сущестует");
            }           
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
