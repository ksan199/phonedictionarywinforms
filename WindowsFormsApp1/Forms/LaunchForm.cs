﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Services;

namespace WindowsFormsApp1
{
    
    public partial class LaunchForm : BaseForm
    {
        private AuthService _authService;

        public enum LaunchDialogResult { Admin, User, None };
        public new LaunchDialogResult DialogResult { get; set; } = LaunchDialogResult.None;

        public LaunchForm()
        {
            InitializeComponent();
            _authService = GetService<AuthService>();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (!_authService.IsAuthenticated)
            {
                LoginForm loginForm = new LoginForm();
                loginForm.ShowDialog();

                switch (loginForm.DialogResult)
                {
                    case System.Windows.Forms.DialogResult.Cancel:
                        return;
                }
            }

            var adminForm = new AdminForm();
            adminForm.Show(this);
            Hide();

        }

        private void observerButton_Click(object sender, EventArgs e)
        {
            var personsForm = new PersonsForm();
            personsForm.Show(this);
            Hide();
        }

        public void onChildFormClose()
        {
            Show();
        }
    }
}
