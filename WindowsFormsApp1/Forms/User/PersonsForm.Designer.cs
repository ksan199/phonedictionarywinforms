﻿namespace WindowsFormsApp1
{
    partial class PersonsForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonsForm));
            this.apartmentNumberLabel = new System.Windows.Forms.GroupBox();
            this.clearButton = new System.Windows.Forms.Button();
            this.regionsComboBox = new System.Windows.Forms.ComboBox();
            this.regionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.phoneDictionaryDataSet = new WindowsFormsApp1.PhoneDictionaryDataSet();
            this.apartmentNumberTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.houseNumberTextBox = new System.Windows.Forms.TextBox();
            this.houseNumberLabel = new System.Windows.Forms.Label();
            this.streetLabel = new System.Windows.Forms.Label();
            this.cityLabel = new System.Windows.Forms.Label();
            this.searchButton = new System.Windows.Forms.Button();
            this.regionLabel = new System.Windows.Forms.Label();
            this.birthdayDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.birthdayLabel = new System.Windows.Forms.Label();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.patronymicLabel = new System.Windows.Forms.Label();
            this.secondNameLabel = new System.Windows.Forms.Label();
            this.patronymicTextBox = new System.Windows.Forms.TextBox();
            this.firstNameLabel = new System.Windows.Forms.Label();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.фамилияDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.имяDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.отчествоDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.деньРожденияDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.регионDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.городDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.номерDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.улицаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.houseNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apartmentNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.номераDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.getPersonsByParamsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.backToMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getPersonsByParamsTableAdapter = new WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters.GetPersonsByParamsTableAdapter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.regionsTableAdapter = new WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters.RegionsTableAdapter();
            this.citiesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.streetsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.citiesTableAdapter = new WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters.CitiesTableAdapter();
            this.streetsTableAdapter = new WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters.StreetsTableAdapter();
            this.citiesComboBox = new System.Windows.Forms.ComboBox();
            this.streetsComboBox = new System.Windows.Forms.ComboBox();
            this.apartmentNumberLabel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.regionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phoneDictionaryDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getPersonsByParamsBindingSource)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.citiesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.streetsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // apartmentNumberLabel
            // 
            this.apartmentNumberLabel.Controls.Add(this.streetsComboBox);
            this.apartmentNumberLabel.Controls.Add(this.citiesComboBox);
            this.apartmentNumberLabel.Controls.Add(this.clearButton);
            this.apartmentNumberLabel.Controls.Add(this.regionsComboBox);
            this.apartmentNumberLabel.Controls.Add(this.apartmentNumberTextBox);
            this.apartmentNumberLabel.Controls.Add(this.label8);
            this.apartmentNumberLabel.Controls.Add(this.houseNumberTextBox);
            this.apartmentNumberLabel.Controls.Add(this.houseNumberLabel);
            this.apartmentNumberLabel.Controls.Add(this.streetLabel);
            this.apartmentNumberLabel.Controls.Add(this.cityLabel);
            this.apartmentNumberLabel.Controls.Add(this.searchButton);
            this.apartmentNumberLabel.Controls.Add(this.regionLabel);
            this.apartmentNumberLabel.Controls.Add(this.birthdayDateTimePicker);
            this.apartmentNumberLabel.Controls.Add(this.birthdayLabel);
            this.apartmentNumberLabel.Controls.Add(this.lastNameTextBox);
            this.apartmentNumberLabel.Controls.Add(this.patronymicLabel);
            this.apartmentNumberLabel.Controls.Add(this.secondNameLabel);
            this.apartmentNumberLabel.Controls.Add(this.patronymicTextBox);
            this.apartmentNumberLabel.Controls.Add(this.firstNameLabel);
            this.apartmentNumberLabel.Controls.Add(this.firstNameTextBox);
            this.apartmentNumberLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.apartmentNumberLabel.Location = new System.Drawing.Point(5, 5);
            this.apartmentNumberLabel.Name = "apartmentNumberLabel";
            this.apartmentNumberLabel.Padding = new System.Windows.Forms.Padding(10);
            this.apartmentNumberLabel.Size = new System.Drawing.Size(750, 143);
            this.apartmentNumberLabel.TabIndex = 8;
            this.apartmentNumberLabel.TabStop = false;
            this.apartmentNumberLabel.Text = "Персональные данные";
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(444, 101);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(75, 23);
            this.clearButton.TabIndex = 11;
            this.clearButton.Text = "Очистить";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // regionsComboBox
            // 
            this.regionsComboBox.DataSource = this.regionsBindingSource;
            this.regionsComboBox.DisplayMember = "Region";
            this.regionsComboBox.FormattingEnabled = true;
            this.regionsComboBox.Location = new System.Drawing.Point(374, 22);
            this.regionsComboBox.Name = "regionsComboBox";
            this.regionsComboBox.Size = new System.Drawing.Size(145, 21);
            this.regionsComboBox.TabIndex = 5;
            this.regionsComboBox.Enter += new System.EventHandler(this.regionsComboBox_Enter);
            // 
            // regionsBindingSource
            // 
            this.regionsBindingSource.DataMember = "Regions";
            this.regionsBindingSource.DataSource = this.phoneDictionaryDataSet;
            this.regionsBindingSource.Sort = "Region asc";
            // 
            // phoneDictionaryDataSet
            // 
            this.phoneDictionaryDataSet.DataSetName = "PhoneDictionaryDataSet";
            this.phoneDictionaryDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // apartmentNumberTextBox
            // 
            this.apartmentNumberTextBox.Location = new System.Drawing.Point(613, 102);
            this.apartmentNumberTextBox.Name = "apartmentNumberTextBox";
            this.apartmentNumberTextBox.Size = new System.Drawing.Size(112, 20);
            this.apartmentNumberTextBox.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(537, 104);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "кв.";
            // 
            // houseNumberTextBox
            // 
            this.houseNumberTextBox.Location = new System.Drawing.Point(613, 76);
            this.houseNumberTextBox.Name = "houseNumberTextBox";
            this.houseNumberTextBox.Size = new System.Drawing.Size(112, 20);
            this.houseNumberTextBox.TabIndex = 8;
            // 
            // houseNumberLabel
            // 
            this.houseNumberLabel.AutoSize = true;
            this.houseNumberLabel.Location = new System.Drawing.Point(537, 79);
            this.houseNumberLabel.Name = "houseNumberLabel";
            this.houseNumberLabel.Size = new System.Drawing.Size(70, 13);
            this.houseNumberLabel.TabIndex = 14;
            this.houseNumberLabel.Text = "Номер дома";
            // 
            // streetLabel
            // 
            this.streetLabel.AutoSize = true;
            this.streetLabel.Location = new System.Drawing.Point(537, 53);
            this.streetLabel.Name = "streetLabel";
            this.streetLabel.Size = new System.Drawing.Size(39, 13);
            this.streetLabel.TabIndex = 13;
            this.streetLabel.Text = "Улица";
            // 
            // cityLabel
            // 
            this.cityLabel.AutoSize = true;
            this.cityLabel.Location = new System.Drawing.Point(537, 25);
            this.cityLabel.Name = "cityLabel";
            this.cityLabel.Size = new System.Drawing.Size(37, 13);
            this.cityLabel.TabIndex = 12;
            this.cityLabel.Text = "Город";
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(327, 101);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(82, 23);
            this.searchButton.TabIndex = 10;
            this.searchButton.Text = "Поиск";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // regionLabel
            // 
            this.regionLabel.AutoSize = true;
            this.regionLabel.Location = new System.Drawing.Point(324, 27);
            this.regionLabel.Name = "regionLabel";
            this.regionLabel.Size = new System.Drawing.Size(43, 13);
            this.regionLabel.TabIndex = 8;
            this.regionLabel.Text = "Регион";
            // 
            // birthdayDateTimePicker
            // 
            this.birthdayDateTimePicker.Location = new System.Drawing.Point(105, 102);
            this.birthdayDateTimePicker.Name = "birthdayDateTimePicker";
            this.birthdayDateTimePicker.ShowCheckBox = true;
            this.birthdayDateTimePicker.Size = new System.Drawing.Size(182, 20);
            this.birthdayDateTimePicker.TabIndex = 4;
            // 
            // birthdayLabel
            // 
            this.birthdayLabel.AutoSize = true;
            this.birthdayLabel.Location = new System.Drawing.Point(13, 106);
            this.birthdayLabel.Name = "birthdayLabel";
            this.birthdayLabel.Size = new System.Drawing.Size(86, 13);
            this.birthdayLabel.TabIndex = 6;
            this.birthdayLabel.Text = "Дата рождения";
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.Location = new System.Drawing.Point(105, 24);
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(182, 20);
            this.lastNameTextBox.TabIndex = 0;
            // 
            // patronymicLabel
            // 
            this.patronymicLabel.AutoSize = true;
            this.patronymicLabel.Location = new System.Drawing.Point(13, 80);
            this.patronymicLabel.Name = "patronymicLabel";
            this.patronymicLabel.Size = new System.Drawing.Size(54, 13);
            this.patronymicLabel.TabIndex = 5;
            this.patronymicLabel.Text = "Отчество";
            // 
            // secondNameLabel
            // 
            this.secondNameLabel.AutoSize = true;
            this.secondNameLabel.Location = new System.Drawing.Point(13, 28);
            this.secondNameLabel.Name = "secondNameLabel";
            this.secondNameLabel.Size = new System.Drawing.Size(56, 13);
            this.secondNameLabel.TabIndex = 0;
            this.secondNameLabel.Text = "Фамилия";
            // 
            // patronymicTextBox
            // 
            this.patronymicTextBox.Location = new System.Drawing.Point(105, 76);
            this.patronymicTextBox.Name = "patronymicTextBox";
            this.patronymicTextBox.Size = new System.Drawing.Size(182, 20);
            this.patronymicTextBox.TabIndex = 3;
            // 
            // firstNameLabel
            // 
            this.firstNameLabel.AutoSize = true;
            this.firstNameLabel.Location = new System.Drawing.Point(13, 54);
            this.firstNameLabel.Name = "firstNameLabel";
            this.firstNameLabel.Size = new System.Drawing.Size(29, 13);
            this.firstNameLabel.TabIndex = 4;
            this.firstNameLabel.Text = "Имя";
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Location = new System.Drawing.Point(105, 50);
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(182, 20);
            this.firstNameTextBox.TabIndex = 2;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AutoGenerateColumns = false;
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.фамилияDataGridViewTextBoxColumn,
            this.имяDataGridViewTextBoxColumn,
            this.отчествоDataGridViewTextBoxColumn,
            this.деньРожденияDataGridViewTextBoxColumn,
            this.регионDataGridViewTextBoxColumn,
            this.городDataGridViewTextBoxColumn,
            this.номерDataGridViewTextBoxColumn,
            this.улицаDataGridViewTextBoxColumn,
            this.houseNumberDataGridViewTextBoxColumn,
            this.apartmentNumberDataGridViewTextBoxColumn,
            this.номераDataGridViewTextBoxColumn});
            this.dataGridView.DataSource = this.getPersonsByParamsBindingSource;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(5, 148);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersWidth = 4;
            this.dataGridView.Size = new System.Drawing.Size(750, 364);
            this.dataGridView.TabIndex = 9;
            this.dataGridView.TabStop = false;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // фамилияDataGridViewTextBoxColumn
            // 
            this.фамилияDataGridViewTextBoxColumn.DataPropertyName = "Фамилия";
            this.фамилияDataGridViewTextBoxColumn.HeaderText = "Фамилия";
            this.фамилияDataGridViewTextBoxColumn.Name = "фамилияDataGridViewTextBoxColumn";
            this.фамилияDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // имяDataGridViewTextBoxColumn
            // 
            this.имяDataGridViewTextBoxColumn.DataPropertyName = "Имя";
            this.имяDataGridViewTextBoxColumn.HeaderText = "Имя";
            this.имяDataGridViewTextBoxColumn.Name = "имяDataGridViewTextBoxColumn";
            this.имяDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // отчествоDataGridViewTextBoxColumn
            // 
            this.отчествоDataGridViewTextBoxColumn.DataPropertyName = "Отчество";
            this.отчествоDataGridViewTextBoxColumn.HeaderText = "Отчество";
            this.отчествоDataGridViewTextBoxColumn.Name = "отчествоDataGridViewTextBoxColumn";
            this.отчествоDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // деньРожденияDataGridViewTextBoxColumn
            // 
            this.деньРожденияDataGridViewTextBoxColumn.DataPropertyName = "День Рождения";
            this.деньРожденияDataGridViewTextBoxColumn.HeaderText = "День Рождения";
            this.деньРожденияDataGridViewTextBoxColumn.Name = "деньРожденияDataGridViewTextBoxColumn";
            this.деньРожденияDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // регионDataGridViewTextBoxColumn
            // 
            this.регионDataGridViewTextBoxColumn.DataPropertyName = "Регион";
            this.регионDataGridViewTextBoxColumn.HeaderText = "Регион";
            this.регионDataGridViewTextBoxColumn.Name = "регионDataGridViewTextBoxColumn";
            this.регионDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // городDataGridViewTextBoxColumn
            // 
            this.городDataGridViewTextBoxColumn.DataPropertyName = "Город";
            this.городDataGridViewTextBoxColumn.HeaderText = "Город";
            this.городDataGridViewTextBoxColumn.Name = "городDataGridViewTextBoxColumn";
            this.городDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // номерDataGridViewTextBoxColumn
            // 
            this.номерDataGridViewTextBoxColumn.DataPropertyName = "Номер";
            this.номерDataGridViewTextBoxColumn.HeaderText = "Номер";
            this.номерDataGridViewTextBoxColumn.Name = "номерDataGridViewTextBoxColumn";
            this.номерDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // улицаDataGridViewTextBoxColumn
            // 
            this.улицаDataGridViewTextBoxColumn.DataPropertyName = "Улица";
            this.улицаDataGridViewTextBoxColumn.HeaderText = "Улица";
            this.улицаDataGridViewTextBoxColumn.Name = "улицаDataGridViewTextBoxColumn";
            this.улицаDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // houseNumberDataGridViewTextBoxColumn
            // 
            this.houseNumberDataGridViewTextBoxColumn.DataPropertyName = "HouseNumber";
            this.houseNumberDataGridViewTextBoxColumn.HeaderText = "Номер дома";
            this.houseNumberDataGridViewTextBoxColumn.Name = "houseNumberDataGridViewTextBoxColumn";
            this.houseNumberDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // apartmentNumberDataGridViewTextBoxColumn
            // 
            this.apartmentNumberDataGridViewTextBoxColumn.DataPropertyName = "ApartmentNumber";
            this.apartmentNumberDataGridViewTextBoxColumn.HeaderText = "Номер квартиры";
            this.apartmentNumberDataGridViewTextBoxColumn.Name = "apartmentNumberDataGridViewTextBoxColumn";
            this.apartmentNumberDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // номераDataGridViewTextBoxColumn
            // 
            this.номераDataGridViewTextBoxColumn.DataPropertyName = "Номера";
            this.номераDataGridViewTextBoxColumn.HeaderText = "Номера";
            this.номераDataGridViewTextBoxColumn.Name = "номераDataGridViewTextBoxColumn";
            this.номераDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // getPersonsByParamsBindingSource
            // 
            this.getPersonsByParamsBindingSource.DataMember = "GetPersonsByParams";
            this.getPersonsByParamsBindingSource.DataSource = this.phoneDictionaryDataSet;
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backToMenuToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(760, 24);
            this.menuStrip.TabIndex = 10;
            this.menuStrip.Text = "menuStrip1";
            // 
            // backToMenuToolStripMenuItem
            // 
            this.backToMenuToolStripMenuItem.Name = "backToMenuToolStripMenuItem";
            this.backToMenuToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.backToMenuToolStripMenuItem.Text = "Назад в меню";
            this.backToMenuToolStripMenuItem.Click += new System.EventHandler(this.backToMenuToolStripMenuItem_Click);
            // 
            // getPersonsByParamsTableAdapter
            // 
            this.getPersonsByParamsTableAdapter.ClearBeforeFill = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dataGridView);
            this.panel1.Controls.Add(this.apartmentNumberLabel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(760, 517);
            this.panel1.TabIndex = 11;
            // 
            // regionsTableAdapter
            // 
            this.regionsTableAdapter.ClearBeforeFill = true;
            // 
            // citiesBindingSource
            // 
            this.citiesBindingSource.DataMember = "Cities";
            this.citiesBindingSource.DataSource = this.phoneDictionaryDataSet;
            this.citiesBindingSource.Sort = "City asc";
            // 
            // streetsBindingSource
            // 
            this.streetsBindingSource.DataMember = "Streets";
            this.streetsBindingSource.DataSource = this.phoneDictionaryDataSet;
            this.streetsBindingSource.Sort = "Street asc";
            // 
            // citiesTableAdapter
            // 
            this.citiesTableAdapter.ClearBeforeFill = true;
            // 
            // streetsTableAdapter
            // 
            this.streetsTableAdapter.ClearBeforeFill = true;
            // 
            // citiesComboBox
            // 
            this.citiesComboBox.DataSource = this.citiesBindingSource;
            this.citiesComboBox.DisplayMember = "City";
            this.citiesComboBox.FormattingEnabled = true;
            this.citiesComboBox.Location = new System.Drawing.Point(580, 22);
            this.citiesComboBox.Name = "citiesComboBox";
            this.citiesComboBox.Size = new System.Drawing.Size(145, 21);
            this.citiesComboBox.TabIndex = 6;
            this.citiesComboBox.Enter += new System.EventHandler(this.citiesComboBox_Enter);
            // 
            // streetsComboBox
            // 
            this.streetsComboBox.DataSource = this.streetsBindingSource;
            this.streetsComboBox.DisplayMember = "Street";
            this.streetsComboBox.FormattingEnabled = true;
            this.streetsComboBox.Location = new System.Drawing.Point(580, 49);
            this.streetsComboBox.Name = "streetsComboBox";
            this.streetsComboBox.Size = new System.Drawing.Size(145, 21);
            this.streetsComboBox.TabIndex = 7;
            this.streetsComboBox.TextChanged += new System.EventHandler(this.streetsComboBox_TextChanged);
            this.streetsComboBox.Enter += new System.EventHandler(this.streetsComboBox_Enter);
            // 
            // PersonsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(760, 541);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MinimumSize = new System.Drawing.Size(740, 580);
            this.Name = "PersonsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "React Native App";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PersonsForm_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.apartmentNumberLabel.ResumeLayout(false);
            this.apartmentNumberLabel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.regionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phoneDictionaryDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getPersonsByParamsBindingSource)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.citiesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.streetsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox apartmentNumberLabel;
        private System.Windows.Forms.ComboBox regionsComboBox;
        private System.Windows.Forms.TextBox apartmentNumberTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox houseNumberTextBox;
        private System.Windows.Forms.Label houseNumberLabel;
        private System.Windows.Forms.Label streetLabel;
        private System.Windows.Forms.Label cityLabel;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Label regionLabel;
        private System.Windows.Forms.DateTimePicker birthdayDateTimePicker;
        private System.Windows.Forms.Label birthdayLabel;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.Label patronymicLabel;
        private System.Windows.Forms.Label secondNameLabel;
        private System.Windows.Forms.TextBox patronymicTextBox;
        private System.Windows.Forms.Label firstNameLabel;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.DataGridView dataGridView;
        private PhoneDictionaryDataSet phoneDictionaryDataSet;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem backToMenuToolStripMenuItem;
        private System.Windows.Forms.BindingSource getPersonsByParamsBindingSource;
        private PhoneDictionaryDataSetTableAdapters.GetPersonsByParamsTableAdapter getPersonsByParamsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn фамилияDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn имяDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn отчествоDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn деньРожденияDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn регионDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn городDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn номерDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn улицаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn houseNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn apartmentNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn номераDataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.BindingSource regionsBindingSource;
        private PhoneDictionaryDataSetTableAdapters.RegionsTableAdapter regionsTableAdapter;
        private System.Windows.Forms.BindingSource citiesBindingSource;
        private System.Windows.Forms.BindingSource streetsBindingSource;
        private PhoneDictionaryDataSetTableAdapters.CitiesTableAdapter citiesTableAdapter;
        private PhoneDictionaryDataSetTableAdapters.StreetsTableAdapter streetsTableAdapter;
        private System.Windows.Forms.ComboBox citiesComboBox;
        private System.Windows.Forms.ComboBox streetsComboBox;
    }
}

