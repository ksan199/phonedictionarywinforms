﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters;

namespace WindowsFormsApp1
{
    public partial class PersonsForm : Form
    {
        /// <summary>
        /// Indicates whether to exit application on form close or show launch menu
        /// </summary>
        public bool IsRelaunchRequied { get; set; }

        public PersonsForm()
        {
            InitializeComponent();
            birthdayDateTimePicker.Checked = false;
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'phoneDictionaryDataSet.Streets' table. You can move, or remove it, as needed.
            this.streetsTableAdapter.Fill(this.phoneDictionaryDataSet.Streets);
            streetsComboBox.Text = "";
            // TODO: This line of code loads data into the 'phoneDictionaryDataSet.Cities' table. You can move, or remove it, as needed.
            this.citiesTableAdapter.Fill(this.phoneDictionaryDataSet.Cities);
            citiesComboBox.Text = "";

            FetchRegionsAndSetToEmpty();
            FetchPersonsByTextBoxValues();
        }

        private void FetchPersonsByTextBoxValues()
        {
            DateTime? birthday = birthdayDateTimePicker.Checked ?
                birthdayDateTimePicker.Value.Date :
                birthday = null;

            getPersonsByParamsTableAdapter.Fill(
                phoneDictionaryDataSet.GetPersonsByParams,
                firstNameTextBox.Text,
                lastNameTextBox.Text,
                patronymicTextBox.Text,
                birthday,
                regionsComboBox.Text,
                citiesComboBox.Text,
                streetsComboBox.Text,
                houseNumberTextBox.Text,
                apartmentNumberTextBox.Text);
        }

        private void PersonsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!IsRelaunchRequied)
            {
                Application.Exit();
            }           
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            FetchPersonsByTextBoxValues();           
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            firstNameTextBox.Text = "";
            lastNameTextBox.Text = "";
            patronymicTextBox.Text = "";
            birthdayDateTimePicker.Checked = false;
            regionsComboBox.Text = "";
            citiesComboBox.Text = "";
            streetsComboBox.Text = "";
            houseNumberTextBox.Text = "";
            apartmentNumberTextBox.Text = "";
        }

        private void streetsComboBox_TextChanged(object sender, EventArgs e)
        {
           if (streetsComboBox.Text == "")
            {
                houseNumberTextBox.Enabled = false;
                houseNumberTextBox.Text = "";
                apartmentNumberTextBox.Enabled = false;
                apartmentNumberTextBox.Text = "";
            }
            else
            {
                houseNumberTextBox.Enabled = true;
                apartmentNumberTextBox.Enabled = true;
            }
        }

        private void backToMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Owner != null)
            {
                IsRelaunchRequied = true;
                ((LaunchForm)Owner).onChildFormClose();
                Close();
            }
        }

        /// <summary>
        /// Загружает список регионов и обнуляет выделенный в поисковике регион
        /// </summary>
        private void FetchRegionsAndSetToEmpty()
        {
            FetchRegions();
            regionsComboBox.Text = "";
        }

        /// <summary>
        /// Загружает список регионов в DataTable
        /// </summary>
        private void FetchRegions()
        {
            this.regionsTableAdapter.Fill(this.phoneDictionaryDataSet.Regions);
        }

        private void regionsComboBox_Enter(object sender, EventArgs e)
        {
            regionsComboBox.DroppedDown = true;
        }

        private void citiesComboBox_Enter(object sender, EventArgs e)
        {
            citiesComboBox.DroppedDown = true;
        }

        private void streetsComboBox_Enter(object sender, EventArgs e)
        {
            streetsComboBox.DroppedDown = true;
        }
    }

}
