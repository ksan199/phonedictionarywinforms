﻿namespace WindowsFormsApp1
{
    partial class LaunchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loginButton = new System.Windows.Forms.Button();
            this.observerButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // loginButton
            // 
            this.loginButton.Location = new System.Drawing.Point(12, 12);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(260, 23);
            this.loginButton.TabIndex = 0;
            this.loginButton.Text = "Администратор";
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // observerButton
            // 
            this.observerButton.Location = new System.Drawing.Point(12, 41);
            this.observerButton.Name = "observerButton";
            this.observerButton.Size = new System.Drawing.Size(260, 23);
            this.observerButton.TabIndex = 1;
            this.observerButton.Text = "Пользователь";
            this.observerButton.UseVisualStyleBackColor = true;
            this.observerButton.Click += new System.EventHandler(this.observerButton_Click);
            // 
            // LaunchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 78);
            this.Controls.Add(this.observerButton);
            this.Controls.Add(this.loginButton);
            this.Name = "LaunchForm";
            this.Text = "LaunchForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button loginButton;
        private System.Windows.Forms.Button observerButton;
    }
}