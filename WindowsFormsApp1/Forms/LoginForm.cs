﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Services;

namespace WindowsFormsApp1
{
    public partial class LoginForm : BaseForm
    {
        private AuthService _authService;

        public LoginForm()
        {
            InitializeComponent();
            Init();

        }

        private void Init()
        {
            _authService = GetService<AuthService>();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            // TODO: Login logic
            if(_authService.Authenticate(loginTextBox.Text, passwordTextBox.Text))
            {
                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                errorLabel.Show();
            }
        }

        private void goBackButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void passwordTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                loginButton.PerformClick();
            }
        }
    }
}
