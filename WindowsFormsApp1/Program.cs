﻿using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Services;

namespace WindowsFormsApp1
{
    public class Program : NinjectProgram
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Kernel = new StandardKernel();
            Kernel.Load(new ExampleModule());

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LaunchForm());
        }
    }

    public class ExampleModule : NinjectModule
    {
        public override void Load()
        {
            Bind<AuthService>().To<AuthService>().InSingletonScope();
            Bind<AccountManager>().To<AccountManager>().InSingletonScope();
        }
    }
}
