﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters;
using static WindowsFormsApp1.PhoneDictionaryDataSet;

namespace WindowsFormsApp1.Services
{
    public class AuthService : BaseService
    {
        private AccountManager _accountManager;

        public static string REDACTOR_ROLE { get; set; } = "Redactor";
        public static string ADMIN_ROLE { get; set; } = "Admin";
        public bool IsAuthenticated { get; set; } = false;
        // Здесь хранятся роли авторизованного пользователя
        public HashSet<string> Roles { get; set; } = new HashSet<string>();
        public QueriesTableAdapter queriesTableAdapter { get; set; } = new QueriesTableAdapter();
        public UsersTableAdapter usersTableAdapter { get; set; } = new UsersTableAdapter();
        public GetAllUsersWithRolesTableAdapter getAllUsersWithRolesTableAdapter { get; set; } = new GetAllUsersWithRolesTableAdapter();

        public AuthService()
        {
            _accountManager = GetService<AccountManager>();
        }

        public bool Authenticate(string login, string password)
        {          
            if (IsAuthenticated)
            {
                return true;
            }

            try
            {
                // Find user by login if any
                var usersDataTable = new UsersDataTable();
                usersTableAdapter.Fill(usersDataTable);
                UsersRow userPasswordRow = usersDataTable.Single(row => row.Username == login);

                var hash = _accountManager.EncryptSHA512(password + userPasswordRow.Salt);
                if (hash == userPasswordRow.HashedPassword)
                {
                    IsAuthenticated = true;
                }
                else
                {
                    return false;
                }

                // Fetch roles for that user
                var usersWithRolesDataTable = new GetAllUsersWithRolesDataTable();
                getAllUsersWithRolesTableAdapter.Fill(usersWithRolesDataTable);
                GetAllUsersWithRolesRow UserRolesRow = usersWithRolesDataTable.Single(row => row.Username == login);

                // Setting roles into variable
                if (UserRolesRow.Admin == 1)
                {
                    Roles.Add(ADMIN_ROLE);
                }
                if (UserRolesRow.Redactor == 1)
                {
                    Roles.Add(REDACTOR_ROLE);
                }
            }
            catch
            {
                IsAuthenticated = false;
                return false;
            }
            
            return true;
        }

        public bool Logout()
        {
            try
            {
                // Log out actions
                IsAuthenticated = false;
                Roles.Clear();
            }
            catch
            {
                IsAuthenticated = true;
                throw new Exception();
            }

            return true;
        }

    }
}
