﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.PhoneDictionaryDataSetTableAdapters;

namespace WindowsFormsApp1.Services
{
    class AccountManager
    {
        private QueriesTableAdapter queriesTableAdapter { get; set; } = new QueriesTableAdapter();
        public void CreateUser(string login, string password, List<string> roles)
        {
            try
            {
                // Create user
                var salt = Guid.NewGuid();
                var encryptedPass = EncryptSHA512(password + salt);
                queriesTableAdapter.InsertUser(login, encryptedPass, salt.ToString());

                // Add roles
                foreach (var role in roles)
                {
                    queriesTableAdapter.BindUserToRole(login, role);
                }
            }
            catch (Exception e)
            {
                throw new NotImplementedException();
            }
        }

        public void DeleteUser(string username)
        {
            try
            {
                // Delete user
                queriesTableAdapter.DeleteUser(username);
            }
            catch
            {
                throw new NotImplementedException();
            }
        }

        public void RemoveRole(string username, string role)
        {
            try
            {
                // Remove user's role
                queriesTableAdapter.UnbindUserFromRole(username, role);
            }
            catch
            {
                throw new NotImplementedException();
            }
        }

        public void AddRole(string username, string role)
        {
            try
            {
                // Add role to user
                queriesTableAdapter.BindUserToRole(username, role);
            }
            catch
            {
                throw new NotImplementedException();
            }
        }

        public string EncryptSHA512(string stringToEncrypt)
        {
            using (SHA512Managed shaM = new SHA512Managed())
            {
                var sBuilder = new StringBuilder();
                var bytesToEncrypt = Encoding.UTF8.GetBytes(stringToEncrypt);
                var enctypted = shaM.ComputeHash(bytesToEncrypt);
                foreach (var symbol in enctypted)
                {
                    sBuilder.Append(symbol.ToString("x2"));
                }
                return sBuilder.ToString();
            }
        }
    }
}
